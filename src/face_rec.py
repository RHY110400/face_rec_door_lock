import numpy as np
import cv2
import pickle

import security

lic_cascade = cv2.CascadeClassifier('cascades/haarcascade_frontalface_alt2.xml')
recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read("trainner2.yml")

cap = cv2.VideoCapture(0)

names = {0:"Rob", 1:"Bojo"}

count = 0

security.access_denied()

while(True):
    #Capture frame by frame
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    lics = lic_cascade.detectMultiScale(gray, scaleFactor=1.5, minNeighbors=5)

    print("frame: :", count)
    count += 1

    for (x,y,w,h) in lics:
        print(x, y, w, h)
        roi_gray = gray[y:y+h, x:x+h]
        roi_color = frame[y:y+h, x:x+h]

        #id__, conf = recognizer.predict(roi_gray)
        #if conf >= 45 and conf <= 85:
        #    print(id_)
        id_, conf = recognizer.predict(roi_gray)
        if conf>= 45 and conf <= 85:

            print(id_)


            font = cv2.FONT_HERSHEY_SIMPLEX
            name = names[id_]
            color = (0, 255, 0)
            stroke = 2
            cv2.putText(frame, name, (x,y), font, 1, color, stroke, cv2.LINE_AA)

            print(id_)
            print(name)
            security.grant_access()



            
        
        color = (255, 0, 0) #BGR
        stroke = 2
        end_cood_x = x + w
        end_cood_y = y + h
        cv2.rectangle(frame, (x, y), (end_cood_x, end_cood_y), color, stroke)

        security.access_denied()
        continue
        #exit(0)
        #color = (255, 0, 0) #BGR
        #stroke = 2
        #end_cood_x = x + w
        #end_cood_y = y + h
        #cv2.rectangle(frame, (x, y), (end_cood_x, end_cood_y), color, stroke)


        

    cv2.imshow('frame', frame)

    if cv2.waitKey(20) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
#cv2.destroyAllWindows()
