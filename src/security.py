from gpiozero import LED
from time import sleep
from RPLCD import CharLCD
import RPi.GPIO as GPIO

# setup LED pins
RED = LED(17)
GREEN = LED(27)



# setup Display pins
lcd = CharLCD(numbering_mode=GPIO.BCM, cols=16, rows=2, pin_rs=21, pin_e=20, pins_data=[16, 12, 1, 7])


def grant_access():
    RED.off()
    GREEN.on()
    lcd.clear()
    lcd.write_string(u'Access Granted!')
    sleep(3)
    lcd.clear()
    lcd.write_string(u'The secret code is...')
    sleep(3)
    lcd.clear()
    lcd.write_string(u'insert code')
    sleep(3)

def access_denied():
    GREEN.off()
    RED.on()
    lcd.clear()
    lcd.write_string(u'Access Denied!')




if __name__ == "__main__":

    while True:
        access_denied()
        sleep(3)
        grant_access()
        sleep(3)


    lcd.clear()
    GPIO.cleanup()
