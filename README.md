### Face Recognition Hacking Demo

The contents of this repository is the source code used to demonstrate 
some of the techniques for hacking facial recognision systems and how 
they can be improved.

This is part of a presentation in which I demonstate how this works and 
how to protect against these exploits.

## Running
The code required to run everything is in the src folder, which is also 
designed to make use of several components of a raspberry pi which can 
all be edited and removed to run on any other PC using the opencv library
and python 3.

